# Projeto: O comportamento do mercado de capitais e as políticas públicas de combate à pandemia na América Latina

## Equipe: O_Brasil_e_o_Mundo

## Descrição:
Situamos nosso tema no impacto da Covid-19 na economia dos países latino-americanos.
Buscamos entender a relação das políticas de combate à pandemia com a variação das bolsas 
de valores dos países. 
Utilizaremos o índice de volatilidade da bolsa calculado em SCOTT et al, 2020. Também seguiremos
a mesma análise feita pelos autores para os Estados Unidos, mas aplicaremos para a América Latina.

## Membros senior:

Bruna de Novais Schultz, 167758,  @Brunanschultz, UNICAMP

Jaume Landazuri, 133583,  @jaumelan, UNICAMP

Yan Prada Moro, 118982, @yanprada, UNICAMP


## Membros junior (se houver): 

Diogo da Silva Gouveia, 2235285, @diogodsg, UTFPR
